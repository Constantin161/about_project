<?php 
    $name = 'Константин';
    $age = '26';
    $about = 'Начинающий web-разработчик-самоучка';
    $e_mail = 'kotradik161@rambler.ru';
    $city = 'Таганрог';
?>
<html>
<head>
    <title> <?php echo $name.' - '.$about?> </title>
    <meta charset="utf-8">
    <style>
        body {
            font-family: sans-serif;  
        }
        
        dl {
            display: table-row;
        }
        
        dt {
            display: table-cell;
            padding: 5px 10px;
        }
    </style>
</head>
<body>
    <h1>Страница пользователя <?= $name?></h1>
    <dl>
        <dt>Имя</dt>
        <dt><?= $name?></dt>
    </dl>
    <dl>
        <dt>Возраст</dt>
        <dt><?= $age?></dt>
    </dl>
    <dl>
        <dt>Адрес электронной почты</dt>
        <dt><a href=<?= "mailto:".$e_mail?>> <?php echo $e_mail?></a></dt>
    </dl>
    <dl>
        <dt>Город</dt>
        <dt><?= $city?></dt>
    </dl>
    <dl>
        <dt>О себе</dt>
        <dt><?php print($about)?></dt>
    </dl>
</body>
</html>